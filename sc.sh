#!/bin/sh

groupId="org.apache.maven.plugins"
artifactId="maven-compiler-plugin"
version="3.5.1"
nexusUser="admin"

classifier=""
type="jar"
repo="nexus_poc"                                                                                                                   
# Nexus 3
base="http://192.168.50.27:8081/repository/nexus_poc/"

if [[ $classifier != "" ]]; then
classifier="-${classifier}"
fi

# Read Password
#echo -n Please enter password for user ${nexusUser}:
#read -s password
#echo
password="********"

filename="${artifactId}-${version}${classifier}.${type}"

if [[ "${version}" == "LATEST" || "${version}" == *SNAPSHOT* ]] ; then
if [[ "${version}" == "LATEST" ]] ; then
version=$(xmllint --xpath "string(//latest)" <(curl -s "${base}/${groupIdUrl}/${artifactId}/maven-metadata.xml"))
fi
timestamp=`curl -u ${nexusUser}:${password} -s "${base}/${groupId}/${artifactId}/${version}/maven-metadata.xml" | xmllint --xpath "string(//timestamp)" -`
buildnumber=`curl -u ${nexusUser}:${password} -s "${base}/${groupId}/${artifactId}/${version}/maven-metadata.xml" | xmllint --xpath "string(//buildNumber)" -`
wget --user ${nexusUser} --password ${password} -P /nexus/artifacts "${base}/${groupId}/${artifactId}/${version}/${artifactId}-${version%-
SNAPSHOT}-${timestamp}-${buildnumber}.${type}"
 else
wget --user ${nexusUser} --password ${password} -P /nexus/artifacts "${base}/${groupId}/${artifactId}/${version}/${artifactId}-${version}${classifier}.${type}"
fi
